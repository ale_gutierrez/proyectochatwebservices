package com.example.agutierrez.proyectochatwebservices;

import com.google.gson.annotations.SerializedName;

/**
 * Created by agutierrez on 27/04/2016.
 */
public class Chat {

    public Chat(){

    }

    @SerializedName("mensaje")

    public String mensaje;


    public Chat(String mensaje){

        this.mensaje = mensaje;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

}
