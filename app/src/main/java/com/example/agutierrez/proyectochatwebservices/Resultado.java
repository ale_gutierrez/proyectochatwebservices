package com.example.agutierrez.proyectochatwebservices;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by agutierrez on 27/04/2016.
 */
public class Resultado {

    @SerializedName("chat")
    public List<Chat> chat;

    @SerializedName("correcto")
    public int correcto;

    public List<Chat> getChat() {
        return chat;
    }

    public void setChat(List<Chat> chat) {
        this.chat = chat;
    }

    public int getCorrecto() {
        return correcto;
    }

    public void setCorrecto(int correcto) {
        this.correcto = correcto;
    }
}
